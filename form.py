from flask_wtf import FlaskForm
from wtforms import StringField, DateField, BooleanField, SelectField
from wtforms.validators import DataRequired, ValidationError
from google.cloud import datastore

datastore_client = datastore.Client()


def validate_name_exist(form, field):
    query = datastore_client.query(kind="gpu")
    if len(list(query.add_filter('__key__', "=", datastore_client.key("gpu", field.data)).fetch())) > 0:
        raise ValidationError("This name is already is the database")


class GPUForm(FlaskForm):
    manuefacturer = StringField('Manuefacturer', validators=[DataRequired()])
    date_issued = DateField('Date Issued', validators=[DataRequired()])
    geometryShader = BooleanField('geometryShader')
    tesselationShader = BooleanField('tesselationShader')
    shaderInt16 = BooleanField('shaderInt16')
    sparseBinding = BooleanField('sparseBinding')
    textureCompressionETC2 = BooleanField('textureCompressionETC2')
    vertexPipelineStoresAndAtomics = BooleanField('vertexPipelineStoresAndAtomics')


class GPUFormInsert(GPUForm):
    name = StringField('Name', validators=[DataRequired(), validate_name_exist])


class GPUFormUpdate(GPUForm):
    name = StringField('Name', validators=[DataRequired()])


class GPUCompare(FlaskForm):
    name1 = SelectField('Name 1', validators=[DataRequired()])
    name2 = SelectField('Name 2', validators=[DataRequired()])


class GPUSearch(FlaskForm):
    choices = [('1', "Unknown"), ('2', "Yes"), ('3', "No")]
    search_geometryShader = SelectField('geometryShader', choices=choices)
    search_tesselationShader = SelectField('tesselationShader', choices=choices)
    search_shaderInt16 = SelectField('shaderInt16', choices=choices)
    search_sparseBinding = SelectField('sparseBinding', choices=choices)
    search_textureCompressionETC2 = SelectField('textureCompressionETC2', choices=choices)
    search_vertexPipelineStoresAndAtomics = SelectField('vertexPipelineStoresAndAtomics', choices=choices)
