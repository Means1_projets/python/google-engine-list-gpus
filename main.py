import os

from flask import Flask, render_template, redirect, url_for, request
from form import GPUFormInsert, GPUFormUpdate, GPUSearch, GPUCompare
from utils import *

firebase_request_adapter = requests.Request()

datastore_client = datastore.Client()

app = Flask(__name__)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY


@app.route('/')
def root():
    return root_with_form(GPUFormInsert(), GPUSearch())


def root_with_form(form, formsearch, query=None):
    claims, error_message = auth(request.cookies.get("token"))

    formcompare = GPUCompare()

    gpus = list(fetch_gpus(query))

    if query is None:
        listgpu = list(map(lambda p: (p.key.name, p.key.name), gpus))
    else:
        listgpu = list(map(lambda p: (p.key.name, p.key.name), fetch_gpus()))
    formcompare.name1.choices = listgpu
    formcompare.name2.choices = listgpu

    return render_template(
        'index.html',
        user_data=claims, error_message=error_message, gpus=gpus, form=form, formsearch=formsearch,
        form_compare=formcompare)


@app.route('/addgpu', methods=["POST"])
def addgpu():
    form = GPUFormInsert()
    claims, error_message = auth(request.cookies.get("token"))

    if claims is not None and form.validate_on_submit():
        store_gpu(form)

        return redirect(url_for('root'))
    return root_with_form(form, GPUSearch())


@app.route('/search', methods=["POST", "GET"])
def search():
    form = GPUSearch()

    if form.validate_on_submit():
        query = datastore_client.query(kind='gpu')
        add_filter(query, form.search_shaderInt16, form.choices)
        add_filter(query, form.search_vertexPipelineStoresAndAtomics, form.choices)
        add_filter(query, form.search_textureCompressionETC2, form.choices)
        add_filter(query, form.search_sparseBinding, form.choices)
        add_filter(query, form.search_tesselationShader, form.choices)
        add_filter(query, form.search_geometryShader, form.choices)
        return root_with_form(GPUFormInsert(), form, query)
    return root_with_form(GPUFormInsert(), form)


@app.route('/compare', methods=["POST"])
def compare():
    form = GPUCompare()
    claims, error_message = auth(request.cookies.get("token"))

    listgpu = list(map(lambda p: (p.key.name, p.key.name), fetch_gpus()))
    form.name1.choices = listgpu
    form.name2.choices = listgpu
    if form.validate_on_submit():
        if form.name1.data == form.name2.data:
            key = list(filter(lambda x: x[0] == form.name1.data, listgpu))[0][1]
            return redirect(url_for('gpu', key=key))

        name1 = list(filter(lambda x: x[0] == form.name1.data, listgpu))[0][1]
        name2 = list(filter(lambda x: x[0] == form.name2.data, listgpu))[0][1]
        gpu1 = list(datastore_client.query(kind='gpu')
                    .add_filter('__key__', "=", datastore_client.key("gpu", name1))
                    .fetch())[0]
        gpu2 = list(datastore_client.query(kind='gpu')
                    .add_filter('__key__', "=", datastore_client.key("gpu", name2))
                    .fetch())[0]
        return render_template(
            'compare.html',
            user_data=claims, error_message=error_message, gpu1=gpu1, gpu2=gpu2)

    return root_with_form(GPUFormInsert(), GPUSearch())


@app.route('/gpu/<key>')
def gpu(key):
    claims, error_message = auth(request.cookies.get("token"))

    query = datastore_client.query(kind="gpu")
    list_gpu = list(query.add_filter('__key__', "=", datastore_client.key("gpu", key)).fetch())
    if len(list_gpu) == 0:
        return redirect(url_for('root'))
    return render_template(
        'gpu.html',
        user_data=claims, error_message=error_message, gpu=list_gpu[0])


@app.route('/gpu/<key>/edit')
def editgpu(key):
    claims, error_message = auth(request.cookies.get("token"))

    form = GPUFormUpdate()
    query = datastore_client.query(kind="gpu")
    list_gpu = list(query.add_filter('__key__', "=", datastore_client.key("gpu", key)).fetch())
    if len(list_gpu) == 0 or claims is None:
        return redirect(url_for('root'))
    gpu = list_gpu[0]

    form.name.data = gpu.key.name
    form.manuefacturer.data = gpu["manuefacturer"]
    form.date_issued.data = gpu["date_issued"]
    form.geometryShader.data = gpu["geometryShader"]
    form.tesselationShader.data = gpu["tesselationShader"]
    form.shaderInt16.data = gpu["shaderInt16"]
    form.sparseBinding.data = gpu["sparseBinding"]
    form.textureCompressionETC2.data = gpu["textureCompressionETC2"]
    form.vertexPipelineStoresAndAtomics.data = gpu["vertexPipelineStoresAndAtomics"]
    return render_template(
        'gpuedit.html',
        user_data=claims, error_message=error_message, gpu=gpu, form=form)


@app.route('/gpu/<key>/edit/send', methods=["POST"])
def editgpusend(key):
    claims, error_message = auth(request.cookies.get("token"))

    if claims is None:
        return redirect(url_for('root'))
    query = datastore_client.query(kind="gpu")
    list_gpu = list(query.add_filter('__key__', "=", datastore_client.key("gpu", key)).fetch())
    if len(list_gpu) == 0:
        return redirect(url_for('root'))
    form = GPUFormUpdate()
    gpu = list_gpu[0]
    if form.validate_on_submit():
        store_gpu(form, False, gpu)

    return redirect(url_for('gpu', key=gpu.key.name))


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
