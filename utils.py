from datetime import datetime
from google.auth.transport import requests
import google.oauth2.id_token
from google.cloud import datastore

datastore_client = datastore.Client()
firebase_request_adapter = requests.Request()


def add_filter(query, field, choices):
    value = list(filter(lambda x: x[0] == field.data, choices))[0][1]
    if value == "Yes" or value == "No":
        query.add_filter(field.name.replace("search_", ""), "=", value == "Yes")


def store_gpu(form, insert=True, gpu=None):
    if insert:
        entity = datastore.Entity(key=datastore_client.key('gpu', form.name.data))
    else:
        entity = gpu
    entity.update({
        'manuefacturer': form.manuefacturer.data,
        'date_issued': datetime.combine(form.date_issued.data, datetime.min.time()),
        'geometryShader': form.geometryShader.data,
        'tesselationShader': form.tesselationShader.data,
        'shaderInt16': form.shaderInt16.data,
        'sparseBinding': form.sparseBinding.data,
        'textureCompressionETC2': form.textureCompressionETC2.data,
        'vertexPipelineStoresAndAtomics': form.vertexPipelineStoresAndAtomics.data
    })
    if entity.key.name != form.name.data:
        datastore_client.delete(entity)
        entity.key = datastore_client.key('gpu', form.name.data)
    datastore_client.put(entity)


def fetch_gpus(query=None):
    if query is None:
        query = datastore_client.query(kind='gpu')

    gpus = query.fetch()

    return gpus


def auth(id_token):
    if id_token:
        try:
            return google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter), None
        except ValueError as exc:
            print(exc)
            return None, str(exc)
    return [None, None]
